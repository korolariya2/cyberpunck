export interface IFeaturesSw {
    id: number;
    name: string;
    description: string;
}