import {IUser} from './IUser';

export interface IProfileSw {
    id: number;
    name: string;
    appearance: string;
    conception: string;
    score: number;
    slogan: string;
    image: string;
    user: IUser;
    profileStatsSw: any;
}
