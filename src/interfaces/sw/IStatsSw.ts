export interface IStatsSw {
    id: number;
    name: string;
    value: number;
    protect?: boolean;
}