import {IStatsSw} from './IStatsSw';

export interface ISkillsSw {
    id: number;
    name: string;
    value: number;
    statsSw: IStatsSw;
}
