export class IDefectsSw {
    id: number;
    name: string;
    description: string;
    type: number;
}