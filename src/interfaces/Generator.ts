import {Profile} from '../entity/Profile';

export interface Gen {
    generate(profile: Profile): Promise<any>;
}
