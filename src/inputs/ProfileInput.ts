import {Profile} from '../entity/Profile';
import {InputObjectType} from 'typegql';

@InputObjectType()
export class ProfileInput extends Profile {

}