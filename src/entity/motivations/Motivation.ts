import {Field, ObjectType} from 'typegql';
import {Column, Entity, ManyToOne, OneToMany} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {MotivationStep} from './MotivationStep';
import {MotivationsLog} from './MotivationsLog';

@Entity()
@ObjectType()
export class Motivation extends DefaultEntity {
    @Column()
    @Field()
    roll: string;

    @ManyToOne(type => MotivationStep, step => step.motivations, {lazy: true})
    @Field({type: () => MotivationStep})
    motivationStep: MotivationStep;

    @OneToMany(type => MotivationsLog, motivationLog => motivationLog.motivation, {
        lazy: true,
    })
    @Field({type: () => [MotivationsLog]})
    motivationLog: MotivationsLog[];
}
