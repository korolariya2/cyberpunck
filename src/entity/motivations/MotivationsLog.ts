import {Field, ObjectType} from 'typegql';
import {Entity, JoinTable, ManyToMany, ManyToOne} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {Motivation} from './Motivation';
import {Profile} from '../Profile';

@Entity()
@ObjectType()
export class MotivationsLog extends DefaultEntity {

    @ManyToOne(type => Profile, profile => profile.motivationsLog, {
        lazy         : true,
    })
    @Field({type: () => Profile})
    profile: Promise<Profile> | Profile;

    @ManyToOne(type => Motivation, motivation => motivation.motivationLog, {
        lazy: true,
    })
    @Field({type: () => Motivation})
    motivation: Motivation;
}
