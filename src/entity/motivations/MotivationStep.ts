import {Field, ObjectType} from 'typegql';
import {Entity, OneToMany} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {Motivation} from './Motivation';

@Entity()
@ObjectType()
export class MotivationStep extends DefaultEntity {
    @OneToMany(type => Motivation, motivations => motivations.motivationStep)
    @Field({type: () => [Motivation]})
    motivations: Motivation[];
}
