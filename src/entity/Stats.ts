import {BaseEntity, Column, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Field, ObjectType} from 'typegql';
import {Profile} from './Profile';
import {Entity} from 'typeorm/decorator/entity/Entity';

@Entity()
@ObjectType()
export class Stats extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number;

    @Field()
    @Column()
    int: number;

    @Field()
    @Column()
    ref: number;

    @Field()
    @Column()
    cool: number;

    @Field()
    @Column()
    tech: number;

    @Field()
    @Column()
    luck: number;

    @Field()
    @Column()
    attr: number;

    @Field()
    @Column()
    ma: number;

    @Field()
    @Column()
    emp: number;

    @Field()
    @Column()
    body: number;

    @Field()
    @Column()
    run: number;

    @Field()
    @Column()
    leap: number;

    @Field()
    @Column()
    lift1: number;

    @Field()
    @Column()
    lift2: number;

    @ManyToOne(type => Profile, profile => profile.stats)
    @Field({type: () => Profile})
    profile: Profile;
}