import {Field, ObjectType} from 'typegql';
import {Column, Entity, ManyToOne, OneToMany} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {RomanticLifeStep} from './RomanticLifeStep';
import {RomanticLifeLog} from './RomanticLifeLog';

@Entity()
@ObjectType()
export class RomanticLife extends DefaultEntity {

    @Column()
    @Field()
    roll: string;

    @ManyToOne(type => RomanticLifeStep, step => step.romanticLife, {lazy: true})
    @Field({type: () => RomanticLifeStep})
    romanticLifeStep: string;

    @Column()
    @Field()
    action: string = '';

    @OneToMany(type => RomanticLifeLog, romanticLifeLog => romanticLifeLog.romanticLife, {
        lazy: true,
    })
    @Field({type: () => [RomanticLifeLog]})
    romanticLifeLog: RomanticLifeLog[];
}
