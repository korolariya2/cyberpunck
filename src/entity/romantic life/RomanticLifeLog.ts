import {Field, ObjectType} from 'typegql';
import {Entity, ManyToMany, ManyToOne} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {LifeEvents} from '../LifeEvents';
import {RomanticLife} from './RomanticLife';

@Entity()
@ObjectType()
export class RomanticLifeLog extends DefaultEntity {

    @ManyToMany(type => LifeEvents, lifeEvents => lifeEvents.romanticLifeLog, {
        cascade: true,
        lazy   : true,
    })
    @Field({type: () => [LifeEvents]})
    lifeEvents: LifeEvents[];

    @ManyToOne(type => RomanticLife, romanticLife => romanticLife.romanticLifeLog, {
        lazy: true,
    })
    @Field({type: () => RomanticLife})
    romanticLife: RomanticLife;
}
