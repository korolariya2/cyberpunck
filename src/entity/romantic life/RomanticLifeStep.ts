import {Field, ObjectType} from 'typegql';
import {Entity, OneToMany} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {RomanticLife} from './RomanticLife';

@Entity()
@ObjectType()
export class RomanticLifeStep extends DefaultEntity {
    @OneToMany(type => RomanticLife, romanticLife => romanticLife.romanticLifeStep)
    @Field({type: () => [RomanticLife]})
    romanticLife: RomanticLife[];
}
