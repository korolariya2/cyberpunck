import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {Field, ObjectType} from 'typegql';
import {Profile} from './Profile';

@Entity()
@ObjectType()
export class Clothes extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number;

    @Column()
    @Field()
    name: string;

    @OneToMany(type => Profile, profile => profile.clothes, {lazy: true})
    @Field({type: () => [Profile]})
    profile: Profile[];
}
