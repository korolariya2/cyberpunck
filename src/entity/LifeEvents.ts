import {BaseEntity, Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Field, ObjectType} from 'typegql';
import {Profile} from './Profile';
import {BpBwLog} from './big problems or big wins/BpBwLog';
import {FaeLog} from './friends&enemies/FaeLog';
import {RomanticLifeLog} from './romantic life/RomanticLifeLog';

@Entity()
@ObjectType()
export class LifeEvents extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number;

    @Column()
    @Field()
    name: string;

    @ManyToOne(type => Profile, profile => profile.lifeEvents)
    @Field({type: () => Profile})
    profile: Profile;

    @ManyToMany(type => BpBwLog, bpBwLog => bpBwLog.lifeEvents, {
        lazy: true,
    })
    @Field({type: () => [BpBwLog]})
    @JoinTable({name: 'le_bp'})
    bpBwLog: Promise<BpBwLog[]> | BpBwLog[];

    @ManyToMany(type => FaeLog, faeLog => faeLog.lifeEvents, {
        lazy: true,
    })
    @Field({type: () => [FaeLog]})
    @JoinTable({name: 'le_fae'})
    faeLog: Promise<FaeLog[]> | FaeLog[];

    @ManyToMany(type => RomanticLifeLog, romanticLifeLog => romanticLifeLog.lifeEvents, {
        lazy: true,
    })
    @Field({type: () => [RomanticLifeLog]})
    @JoinTable({name: 'le_rl'})
    romanticLifeLog: Promise<RomanticLifeLog[]> | RomanticLifeLog[];
}
