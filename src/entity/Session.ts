import {BaseEntity, Entity, Index, JoinColumn, OneToOne, PrimaryColumn} from 'typeorm';
import {Field, ObjectType} from 'typegql';
import {User} from './User';

@Entity()
@ObjectType()
export class Session extends BaseEntity {

    @PrimaryColumn()
    @Index({unique: true})
    @Field()
    token: string;

    /* tslint:disable */
    @OneToOne(type => User, {lazy: true})
    /* tslint:enable */
    @JoinColumn()
    @Field({type: () => User})
    user: User;

    dateExpired: number;
}
