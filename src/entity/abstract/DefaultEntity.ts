import {BaseEntity, Column, PrimaryGeneratedColumn} from 'typeorm';
import {Field} from 'typegql';

export abstract class DefaultEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number;

    @Column()
    @Field()
    name: string;
}
