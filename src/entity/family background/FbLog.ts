import {Field, ObjectType} from 'typegql';
import {Entity, ManyToOne} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {Profile} from '../Profile';
import {Fb} from './Fb';

@Entity()
@ObjectType()
export class FbLog extends DefaultEntity {

    @ManyToOne(type => Profile, profile => profile.fbLog, {
        lazy: true,
    })
    @Field({type: () => Profile})
    profile: Profile;

    @ManyToOne(type => Fb, fb => fb.fbLog, {
        lazy: true,
    })
    @Field({type: () => Fb})
    fb: Fb;
}
