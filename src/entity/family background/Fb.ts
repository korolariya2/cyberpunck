import {Field, ObjectType} from 'typegql';
import {Column, Entity, ManyToOne, OneToMany} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {FbLog} from './FbLog';
import {FbStep} from './FbStep';

@Entity()
@ObjectType()
export class Fb extends DefaultEntity {
    @Column()
    @Field()
    roll: string;

    @ManyToOne(type => FbStep, step => step.fb, {lazy: true})
    @Field({type: () => FbStep})
    fbStep: FbStep;

    @OneToMany(type => FbLog, fbLog => fbLog.fb, {
        lazy: true,
    })
    @Field({type: () => [FbLog]})
    fbLog: FbLog[];

    @Column({ nullable: true })
    @Field()
    nextStep: string;

    @Column({ nullable: true })
    @Field()
    action: string;
}
