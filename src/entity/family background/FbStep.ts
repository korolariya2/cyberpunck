import {Field, ObjectType} from 'typegql';
import {Entity, OneToMany} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {Fb} from './Fb';

@Entity()
@ObjectType()
export class FbStep extends DefaultEntity {
    @OneToMany(type => Fb, fb => fb.fbStep, {
        lazy: true,
    })
    @Field({type: () => [Fb]})
    fb: Fb[];
}
