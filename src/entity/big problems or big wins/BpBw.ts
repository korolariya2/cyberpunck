import {Field, ObjectType} from 'typegql';
import {Column, Entity, ManyToOne, OneToMany} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {BpBwStep} from './BpBwStep';
import {BpBwLog} from './BpBwLog';
import {BpBwAction} from './BpBwAction';

@Entity()
@ObjectType()
export class BpBw extends DefaultEntity {
    @Column()
    @Field()
    roll: string;

    @ManyToOne(type => BpBwStep, step => step.bpBw, {lazy: true})
    @Field({type: () => BpBwStep})
    bpBwStep: BpBwStep;

    @OneToMany(type => BpBwLog, bpBwLog => bpBwLog.bpBw, {
        lazy: true,
    })
    @Field({type: () => [BpBwLog]})
    bpBwLog: BpBwLog[];

    @OneToMany(type => BpBwAction, bpBwActions => bpBwActions.bpBw, {
        lazy: true,
    })
    @Field({type: () => [BpBwAction]})
    bpBwActions: BpBwAction[];
}
