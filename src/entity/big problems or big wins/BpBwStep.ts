import {Field, ObjectType} from 'typegql';
import {Entity, OneToMany} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {BpBw} from './BpBw';

@Entity()
@ObjectType()
export class BpBwStep extends DefaultEntity {
    @OneToMany(type => BpBw, bpBw => bpBw.bpBwStep)
    @Field({type: () => [BpBw]})
    bpBw: BpBw[];
}
