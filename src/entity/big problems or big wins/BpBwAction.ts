import {Field, ObjectType} from 'typegql';
import {Column, Entity, ManyToOne} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {BpBw} from './BpBw';

@Entity()
@ObjectType()
export class BpBwAction extends DefaultEntity {
    @ManyToOne(type => BpBw, bpBw => bpBw.bpBwActions, {lazy: true})
    @Field({type: () => BpBw})
    bpBw: BpBw;

    @Column()
    @Field()
    action: string = '';

    @Column()
    @Field()
    roll: string;
}
