import {Field, ObjectType} from 'typegql';
import {Entity, ManyToMany, ManyToOne} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {LifeEvents} from '../LifeEvents';
import {BpBw} from './BpBw';

@Entity()
@ObjectType()
export class BpBwLog extends DefaultEntity {
    @ManyToMany(type => LifeEvents, lifeEvents => lifeEvents.bpBwLog, {
        lazy: true,
    })
    @Field({type: () => [LifeEvents]})
    lifeEvents: Promise<LifeEvents[]> | LifeEvents[];

    @ManyToOne(type => BpBw, bpBw => bpBw.bpBwLog, {
        lazy: true,
    })
    @Field({type: () => BpBw})
    bpBw: BpBw;
}
