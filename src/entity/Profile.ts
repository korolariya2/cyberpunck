import {Entity} from 'typeorm/decorator/entity/Entity';
import {BaseEntity, Column, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {Field, InputField, InputObjectType, ObjectType} from 'typegql';
import {User} from './User';
import {Clothes} from './Clothes';
import {Hairstyle} from './Hairstyle';
import {Affectations} from './Affectations';
import {EthnicOrigins} from './EthnicOrigins';
import {LifeEvents} from './LifeEvents';
import {MotivationsLog} from './motivations/MotivationsLog';
import {FbLog} from './family background/FbLog';
import {Stats} from './Stats';

@Entity()
@ObjectType()
export class Profile extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number;

    @Column()
    @InputField()
    @Field()
    name: string;

    @OneToMany(type => MotivationsLog, motivationsLog => motivationsLog.profile, {
        cascade: true,
        lazy   : true,
    })
    @Field({type: () => [MotivationsLog]})
    motivationsLog: Promise<MotivationsLog[]> | MotivationsLog[];

    @OneToMany(type => FbLog, fbLog => fbLog.profile, {
        cascade: true,
        lazy   : true,
    })
    @Field({type: () => [FbLog]})
    fbLog: Promise<FbLog[]> | FbLog[];

    @OneToMany(type => LifeEvents, lifeEvents => lifeEvents.profile, {
        cascade: true,
        lazy   : true,
    })
    @Field({type: () => [LifeEvents]})
    lifeEvents: Promise<LifeEvents[]> | LifeEvents[];

    @OneToMany(type => Stats, stats => stats.profile, {
        cascade: true,
        lazy   : true,
    })
    @Field({type: () => [Stats]})
    stats: Promise<Stats[]> | Stats[];

    @ManyToOne(type => User, user => user.profile, {lazy: true})
    @Field({type: () => User})
    user: User;

    @ManyToOne(type => Clothes, clothes => clothes.profile, {lazy: true})
    @Field({type: () => Clothes})
    clothes: Clothes;

    @ManyToOne(type => Hairstyle, hairstyle => hairstyle.profile, {lazy: true})
    @Field({type: () => Hairstyle})
    hairstyle: Hairstyle;

    @ManyToOne(type => Affectations, affectations => affectations.profile, {lazy: true})
    @Field({type: () => Affectations})
    affectations: Affectations;

    @ManyToOne(type => EthnicOrigins, ethnicOrigins => ethnicOrigins.profile, {lazy: true})
    @Field({type: () => EthnicOrigins})
    ethnicOrigins: EthnicOrigins;

    @Column()
    @InputField()
    @Field()
    age: string = '16';

    @Column({nullable: true})
    @InputField()
    @Field()
    sex: string = null;

    @Column({nullable: true})
    @InputField()
    @Field()
    relationship: string = null;

    @ManyToMany(type => Profile, profile => profile.siblings, {
        cascade: ['insert'],
        lazy   : true,
    })
    @Field({type: () => [Profile]})
    @JoinTable({name: 'profile_sibling'})
    siblings: Promise<Profile[]> | Profile[];

    @Column()
    @InputField()
    @Field()
    cache: number = 0;
}
