import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany, Index, Unique} from 'typeorm';
import {Field, InputField, ObjectType} from 'typegql';
import {Profile} from './Profile';
import {ProfileSw} from './savageworld/ProfileSw';

@Entity()
@ObjectType()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number;

    @Column()
    @Index({ unique: true })
    @Field()
    @InputField()
    username: string;

    @Column()
    @Field()
    @InputField()
    password: string;

    @OneToMany(type => Profile, profile => profile.user, {lazy: true})
    @Field({type: () => [Profile]})
    profile: Profile[];

    @OneToMany(type => ProfileSw, profileSw => profileSw.user, {lazy: true})
    @Field({type: () => [ProfileSw]})
    profileSw: ProfileSw[];
}
