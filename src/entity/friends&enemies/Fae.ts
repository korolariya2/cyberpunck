import {Field, ObjectType} from 'typegql';
import {Column, Entity, ManyToOne, OneToMany} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {FaeStep} from './FaeStep';
import {FaeLog} from './FaeLog';

//Friend And Enemies
@Entity()
@ObjectType()
export class Fae extends DefaultEntity {

    @Column()
    @Field()
    roll: string;

    @ManyToOne(type => FaeStep, step => step.fae, {lazy: true})
    @Field({type: () => FaeStep})
    faeStep: FaeStep;

    @Column()
    @Field()
    action: string = '';

    @OneToMany(type => FaeLog, bpBwLog => bpBwLog.fae, {
        lazy: true,
    })
    @Field({type: () => [FaeLog]})
    faeLog: FaeLog[];
}
