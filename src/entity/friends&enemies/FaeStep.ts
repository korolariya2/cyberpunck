import {Field, ObjectType} from 'typegql';
import {Entity, OneToMany} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {Fae} from './Fae';

@Entity()
@ObjectType()
export class FaeStep extends DefaultEntity {
    @OneToMany(type => Fae, fae => fae.faeStep)
    @Field({type: () => [Fae]})
    fae: Fae[];
}
