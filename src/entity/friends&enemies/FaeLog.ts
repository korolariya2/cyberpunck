import {Field, ObjectType} from 'typegql';
import {Entity, ManyToMany, ManyToOne} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {LifeEvents} from '../LifeEvents';
import {Fae} from './Fae';

@Entity()
@ObjectType()
export class FaeLog extends DefaultEntity {

    @ManyToMany(type => LifeEvents, lifeEvents => lifeEvents.faeLog, {
        cascade: true,
        lazy   : true,
    })
    @Field({type: () => [LifeEvents]})
    lifeEvents: LifeEvents[];

    @ManyToOne(type => Fae, fae => fae.faeLog, {
        lazy: true,
    })
    @Field({type: () => Fae})
    fae: Fae;
}
