import {Field, InputField, ObjectType} from 'typegql';
import {
    BaseEntity,
    Column,
    Entity, JoinTable,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import {User} from '../User';
import {ProfileStatsSw} from './ProfileStatsSw';

@Entity()
@ObjectType()
export class ProfileSw extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number;

    @ManyToOne(type => User, user => user.profileSw, {lazy: true})
    @Field({type: () => User})
    user: User;

    @Column()
    @InputField()
    @Field()
    name: string;

    @Column({nullable: true})
    @InputField()
    @Field()
    conception: string;

    @Column({nullable: true})
    @InputField()
    @Field()
    appearance: string;

    @Column({nullable: true})
    @InputField()
    @Field()
    slogan: string;

    // @ManyToMany((type) => StatsSw, (stats) => stats.profiles, {
    //     lazy: true,
    // })
    // @JoinTable()
    // @Field({type: () => [StatsSw]})
    // stats: Promise<StatsSw[]> | StatsSw[];

    @Column({default: 500})
    @Field()
    score: number = 500;

    @InputField({isNullable: true})
    @Column({default: null})
    @Field()
    image: string;

    @OneToMany(type => ProfileStatsSw, (profileStatsSw) => profileStatsSw.profile) // note: we will create author property in the Photo class below
    @JoinTable()
    @Field({type: () => [ProfileStatsSw]})
    profileStatsSw: ProfileStatsSw[];
}
