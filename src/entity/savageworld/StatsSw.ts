import {Column, Entity, ManyToMany, OneToMany, OneToOne} from 'typeorm';
import {Field, ObjectType} from 'typegql';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {IStatsSw} from '../../interfaces/sw/IStatsSw';
import {ProfileStatsSw} from './ProfileStatsSw';

@Entity()
@ObjectType()
export class StatsSw extends DefaultEntity implements IStatsSw {
    // @OneToOne(type => ProfileSw, profile => profile.stats, {lazy: true})
    // @Field({type: () => ProfileSw})
    // profile: ProfileSw;

    @Column({default: 4})
    @Field()
    value: number = 4;

    @Column({default: false})
    @Field()
    protect: boolean = false;

    @OneToMany(type => ProfileStatsSw, profileStatsSw => profileStatsSw.stats) // note: we will create author property in the Photo class below
    @Field({type: () => [ProfileStatsSw]})
    profileStatsSw: ProfileStatsSw[];

    // @OneToMany(type => SkillsSw, skills => skills.statsSw, {lazy: true})
    // @Field({type: () => [SkillsSw]})
    // skills: SkillsSw[];

    // @ManyToMany((type) => ProfileSw, (profiles) => profiles.stats)
    // profiles: ProfileSw[];
}
