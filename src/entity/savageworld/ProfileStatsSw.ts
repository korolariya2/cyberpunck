import {Field, ObjectType} from 'typegql';
import {BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {ProfileSw} from './ProfileSw';
import {StatsSw} from './StatsSw';

@Entity()
@ObjectType()
export class ProfileStatsSw extends BaseEntity {
    @PrimaryGeneratedColumn()
    @Field()
    id: number;

    @Column({default: 4})
    @Field()
    value: number = 4;

    @ManyToOne(type => ProfileSw, (profile) => profile.profileStatsSw)
    @Field({type: () => ProfileSw})
    profile: ProfileSw;

    @ManyToOne(type => StatsSw, (stats) => stats.profileStatsSw)
    @Field({type: () => StatsSw})
    stats: StatsSw;
}
