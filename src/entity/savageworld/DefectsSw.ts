import {Before, Field, ObjectType} from 'typegql';
import {Column, Entity} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';

@Entity()
@ObjectType()
export class DefectsSw extends DefaultEntity {

    @Column({nullable: true})
    @Field()
    description: string;

    @Column({nullable: true})
    @Field()
    type: number;
}
