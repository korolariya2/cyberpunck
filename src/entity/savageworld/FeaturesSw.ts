import {Field, ObjectType} from 'typegql';
import {Column, Entity} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';

@Entity()
@ObjectType()
export class FeaturesSw extends DefaultEntity {

    @Column({nullable: true})
    @Field()
    description: string;

    @Column()
    @Field()
    level: number;

    @Column({nullable: true})
    @Field()
    restriction: string;
}
