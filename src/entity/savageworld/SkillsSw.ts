import {Column, Entity, ManyToOne} from 'typeorm';
import {Field, ObjectType} from 'typegql';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {StatsSw} from "./StatsSw";

@Entity()
@ObjectType()
export class SkillsSw extends DefaultEntity {
    @Column({default: 0})
    @Field()
    value: number = 0;

    @ManyToOne(type => StatsSw, {lazy: true})
    @Field({type: () => StatsSw})
    statsSw: StatsSw;
}
