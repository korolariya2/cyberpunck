import {InputField, InputObjectType} from 'typegql';

@InputObjectType()
export class StatsSwInput {
    @InputField()
    stats: number;
    @InputField()
    profile: number;
    @InputField()
    value: number;
}
