import {Field, ObjectType} from 'typegql';
import {Column, Entity, ManyToOne, OneToMany} from 'typeorm';
import {DefaultEntity} from '../abstract/DefaultEntity';
import {LevelSw} from './LevelSw';

@Entity()
@ObjectType()
export class StrengthSw extends DefaultEntity {

    @ManyToOne(type => LevelSw)
    @Field({type: () => LevelSw})
    levelSw: LevelSw;

    @Column()
    @Field()
    power: string;

    @Column()
    @Field()
    length: string;

    @Column()
    @Field()
    long: string;

    @Column({type: 'longtext'})
    @Field()
    desc: string;

    @Column({type: 'longtext', nullable: true})
    @Field()
    additional: string;
}