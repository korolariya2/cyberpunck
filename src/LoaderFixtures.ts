import * as fs from 'fs';
import * as path from 'path';
import * as jsonFile from 'jsonfile';
import {CON} from './index';

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
}

export class LoaderFixtures {
    constructor() {
    }

    static queue = [
        'DataFaeStep.json',
        'DataFae.json',
        'DataProfile.json',
        'DataFamily.json',
        'DataBpBwStep.json',
        'DataBpBw.json',
        'DataBpBwAction.json',
        'DataRomanticLifeStep.json',
        'DataRomanticLife.json',
        'DataFbStep.json',
        'DataFb.json',
        'DataStatsSw.json',
        'DataSkillsSw.json',
        'DataLevelSw.json',
        'DataStrengthSw.json',
    ];

    static load() {
        const fixturesDir = __dirname + '/fixtures/en';
        const files       = fs.readdirSync(path.normalize(fixturesDir));
        files.sort(function (a, b) {
            if (LoaderFixtures.queue.indexOf(a) > LoaderFixtures.queue.indexOf(b)) {
                return 1;
            }
            if (LoaderFixtures.queue.indexOf(a) < LoaderFixtures.queue.indexOf(b)) {
                return -1;
            }

            return 0;
        });

        (async () => {
            await asyncForEach(files, async (fileName) => {
                console.log(fileName);
                const filePath = path.normalize(fixturesDir + '/' + fileName);
                const objName  = fileName.replace(/Data(.*)\.json/, '$1');
                const link     = CON.manager.getRepository(objName).target;

                if (fs.lstatSync(filePath).isFile()) {
                    const objs = jsonFile.readFileSync(filePath);

                    await asyncForEach(objs, async (data) => {
                        const obj = Object.assign(new link(), data);

                        for (const key in data) {
                            if (data.hasOwnProperty(key) && (typeof(data[key]) === 'string') && (data[key].indexOf('findOneById') !== -1)) {
                                obj[key] = await CON.manager.getRepository(capitalize(key)).findOne({id: data[key].split(' ')[1]});
                            }
                        }

                        await obj.save(obj)
                            .catch((e) => {
                                console.log(e);
                            });
                    });
                }
            });
        })();
    }
}
