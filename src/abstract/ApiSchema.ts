import {asyncForEach} from '../LoaderFixtures';
import {StatsGen} from '../generator/StatsGen';
import {LifeEventsGen} from '../generator/LifeEventsGen';
import {Profile} from '../entity/Profile';
import {DPSGen} from '../generator/DPSGen';
import {Mutation, Query, SchemaRoot} from 'typegql';
import {FbGen} from '../generator/FbGen';
import {MotivationsGen} from '../generator/MotivationsGen';

@SchemaRoot()
export class ApiSchema {

    @Query({type: Profile})
    public async getProfile(id: number): Promise<Profile> {
        // return await getConnection()
        //     .createQueryBuilder()
        //     .select('p')
        //     .from(Profile, 'p')
        //     .leftJoinAndSelect('p.motivationsLog', 'ml')
        //     .leftJoinAndSelect('p.clothes', 'cl')
        //     .where('p.id = :id', {id: id})
        //     .getOne();
        return await Profile.findOne({id: id});
    }

    @Query({type: [Profile]})
    async getProfiles(): Promise<Profile[]> {
        return await Profile.find();
    }

    @Mutation({type: Profile})
    public async generateStats(id: number): Promise<Profile> {
        const profile = await Profile.findOne({id: id});

        const statsGen = new StatsGen();
        await statsGen.generate(profile);

        await profile.save();
        return profile;
    }

    @Mutation({type: Profile})
    public async generateDressPersonalStyle(id: number): Promise<Profile> {
        const profile = await Profile.findOne({id: id});
        const dps     = new DPSGen();
        await dps.create(profile);
        await profile.save();
        return profile;
    }

    @Mutation({type: Profile})
    public async generateMotivations(id: number): Promise<Profile> {
        const profile = await Profile.findOne({id: id});
        const ml      = await profile.motivationsLog;
        await asyncForEach(ml, async item => await item.remove());
        const mg = new MotivationsGen();
        await mg.generate(profile);
        return profile;
    }

    @Mutation({type: Profile})
    public async generateLifeEvents(id: number): Promise<Profile> {
        let profile = await Profile.findOne({id: id});
        const le    = await profile.lifeEvents;
        await asyncForEach(le, async item => await item.remove());

        const leg = new LifeEventsGen();
        await leg.generate(profile);
        await profile.save();
        return profile;
    }

    @Mutation({type: Profile})
    public async createProfile(name: string): Promise<Profile> {
        let profile = await Profile.findOne({name: name});
        if (!profile) {
            profile      = new Profile();
            profile.name = name;
            await profile.save();
        }
        return profile;
    }

    @Mutation({type: Profile})
    public async generateFb(id: number): Promise<Profile> {
        let profile = await Profile.findOne({id: id});
        const fbl   = await profile.fbLog;
        await asyncForEach(fbl, async item => await item.remove());
        profile.siblings = [];
        const fbGen      = new FbGen();
        profile          = await fbGen.generate(profile);
        await profile.save();
        return profile;
    }

    @Mutation({type: Profile})
    public async generateBpBw(id: number): Promise<Profile> {
        const profile = await Profile.findOne({id: id});

        return profile;
    }
}