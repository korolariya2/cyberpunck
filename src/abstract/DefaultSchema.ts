import {Mutation, Query, SchemaRoot} from 'typegql';
import {plainToClass} from 'class-transformer';
import {User} from '../entity/User';
import {UserInput} from '../inputs/UserInput';

@SchemaRoot()
export class DefaultSchema {
    @Mutation({type: User})
    public async createUser(obj: UserInput): Promise<User> {
        const user = plainToClass(User, obj);
        await user.save();
        return user;
    }

    @Query({type: User})
    public async getUser(id: number): Promise<User> {
        return await User.findOne({id: id});
    }
}