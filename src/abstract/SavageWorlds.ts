import {Arg, Before, Context, Mutation, Query, SchemaRoot} from 'typegql';
import {plainToClass} from 'class-transformer';
import {ProfileSw} from '../entity/savageworld/ProfileSw';
import {ProfileSwInput} from '../entity/savageworld/inputs/ProfileSwInput';
import {
    GraphQLString,
    GraphQLFloat,
    GraphQLBoolean,
    isNamedType,
    getNamedType,
    GraphQLEnumType,
} from 'graphql';
import {StatsSw} from '../entity/savageworld/StatsSw';
import {SkillsSw} from '../entity/savageworld/SkillsSw';
import {FeaturesSw} from '../entity/savageworld/FeaturesSw';
import {DefectsSw} from '../entity/savageworld/DefectsSw';
import {Cookies} from 'cookie';

import * as crypto from 'crypto';
import {UserInput} from '../inputs/UserInput';
import {User} from '../entity/User';
import {Session} from '../entity/Session';
import {CON} from '../index';
import {StatsSwInput} from '../entity/savageworld/inputs/StatsSwInput';
import {ProfileStatsSw} from '../entity/savageworld/ProfileStatsSw';
import {getConnection} from "typeorm";

const obj = {
    id: 'test',
    auth: false,
    isAdmin: () => this.auth,
};

function AuthOnly(errorMessage: string = 'Unauthorized') {
    return Before(async ({context}) => {
        const session = await  Session.findOne({token: context.cookies.AuthToken});
        if (session) {
            return;
        }

        context.res.status(403);

        throw new Error(errorMessage);
    });
}

// function Auth(errorMessage: string = 'Unauthorized') {
//     return Before(async ({context}) => {
//         const promiseToken = new Promise((resolve, reject) => {
//             crypto.randomBytes(48, (err, buffer) => {
//                 resolve(buffer.toString('hex'));
//             });
//         });
//         const token =  await promiseToken;
//         context.res.cookie('AuthToken', token);
//
//     });
// }

@SchemaRoot()
export class SavageWorlds {
    @AuthOnly()
    @Mutation({type: ProfileSw, description: 'This action create profile for Savage World'})
    public async createProfileSw(profileObject: ProfileSwInput, @Context context): Promise<ProfileSw> {
        const profile = plainToClass(ProfileSw, profileObject);
        profile.user = await SavageWorlds.getUser(context);
        await profile.save();
        return profile;
    }

    @AuthOnly()
    @Mutation({type: ProfileStatsSw, description: 'This action create profile for Savage World'})
    public async saveStatsSw(profileStatsObject: StatsSwInput): Promise<ProfileStatsSw> {
        const pss = await getConnection()
            .createQueryBuilder()
            .delete()
            .from(User)
            .where('profile = :profile', {profile: profileStatsObject.profile})
            .andWhere('stats = :stats', {stats: profileStatsObject.stats})
            .execute();

        const profileStatsSw: ProfileStatsSw = plainToClass(ProfileStatsSw, profileStatsObject) as ProfileStatsSw;
        return await profileStatsSw.save();
    }

    @AuthOnly()
    @Query({type: [ProfileSw], name: 'Profile SW', description: 'This action return profile Savage World'})
    public async getProfilesSw(@Context context): Promise<ProfileSw[]> {
        const session = await  Session.findOne({token: context.cookies.AuthToken}) as Session;
        if (session) {
            const signedUser: User = await session.user;
            return await signedUser.profileSw;
        }
        return null;
    }

    @AuthOnly()
    @Query({type: ProfileSw, name: 'Profile SW', description: 'This action return profile Savage World'})
    public async getProfileSw(id: number): Promise<ProfileSw> {
        return await CON
            .getRepository(ProfileSw)
            .createQueryBuilder('profile')
            .leftJoinAndSelect('profile.profileStatsSw', 'profileStatsSw')
            .leftJoinAndSelect('profile.user', 'user')
            .leftJoinAndSelect('profileStatsSw.stats', 'stats')
            .where(`profile.id = ${id}`)
            .getOne();
    }

    @AuthOnly()
    @Query({type: [StatsSw], name: 'Stats SW', description: 'This action return stats Savage World'})
    public async getStatsSw(): Promise<StatsSw[]> {
        return await StatsSw.find() as StatsSw[];
    }

    @AuthOnly()
    @Query({type: [SkillsSw], name: 'Skills SW', description: 'This action return skills Savage World'})
    public async getSkillsSw(): Promise<SkillsSw[]> {
        return await SkillsSw.find() as SkillsSw[];
    }

    @AuthOnly()
    @Query({type: [FeaturesSw], name: 'Features SW', description: 'This action return features Savage World'})
    public async getFeaturesSw(): Promise<FeaturesSw[]> {
        return await FeaturesSw.find() as FeaturesSw[];
    }

    @Query({type: [DefectsSw], name: 'Defects SW', description: 'This action return defects Savage World'})
    public async getDefectsSw(): Promise<DefectsSw[]> {
        return await DefectsSw.find() as DefectsSw[];
    }

    @Mutation({type: User, description: 'This action for registration user'})
    public async registration(userObject: UserInput, @Context context): Promise<User> {
        const token = await (new Promise((resolve, reject) => {
            crypto.randomBytes(48, (err, buffer) => {
                resolve(buffer.toString('hex'));
            });
        })) as string;

        const user = plainToClass(User, userObject);
        await user.save();

        const session = new Session();
        session.user = user;
        session.token = token;
        await session.save();
        context.res.cookie('AuthToken', token);

        return user;
    }

    @Mutation({type: User, description: 'This action identification user'})
    public async auth(userObject: UserInput, @Context context): Promise<User> {
        const session = await  Session.findOne({token: context.cookies.AuthToken});
        if (session) {
            const signedUser = await session.user;
            if (signedUser) {
                return signedUser;
            }
        }

        const foundUser = await User.findOne({username: userObject.username}) as User;
        if (foundUser) {
            if (foundUser.password !== userObject.password) {
                throw new Error('Password incorrect!');
            }

            const oldSession = await  Session.findOne({user: foundUser}) as Session;
            if (oldSession) {
                await oldSession.remove();
            }

            const token = await (new Promise((resolve, reject) => {
                crypto.randomBytes(48, (err, buffer) => {
                    resolve(buffer.toString('hex'));
                });
            })) as string;

            const newSession = new Session();
            newSession.user = foundUser;
            newSession.token = token;
            await newSession.save();
            context.res.cookie('AuthToken', token);

            delete foundUser.password;
            context.res.cookie('LoggedUser', JSON.stringify(foundUser));

            return foundUser;
        }
        throw new Error('User not found!');
    }

    public static async getUser(context) {
        const session: Session = await Session.findOne({token: context.cookies.AuthToken}) as Session;
        if (session) {
            return await
                session.user;
        }
        return null;
    }
}
