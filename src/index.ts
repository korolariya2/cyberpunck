import * as cors from 'cors';
import * as express from 'express';
import {
    Before,
    compileSchema,
} from 'typegql';
import * as graphqlHTTP from 'express-graphql';
import * as http from 'http';
import {createConnection} from 'typeorm';
import {LoaderFixtures} from './LoaderFixtures';
import {SavageWorlds} from './abstract/SavageWorlds';
import {ApiSchema} from './abstract/ApiSchema';
import {DefaultSchema} from './abstract/DefaultSchema';
import * as cookieParser from 'cookie-parser';
import * as fileUpload from 'express-fileupload';

const app    = express();
const server = new http.Server(app);
const io     = require('socket.io')(server);

// server.listen(8085);

// app.use(express.static(path.join(__dirname, 'front-app/dist/')));
// app.get('/', function (req, res) {
//     res.sendfile(__dirname + '/front-app/dist/index.html');
// });

export let CON;

createConnection().then(async connection => {
    CON = connection;
    await CON.synchronize(true);
    LoaderFixtures.load();
});

const compiledSchema = compileSchema({roots: [ApiSchema, DefaultSchema, SavageWorlds]});

const corsOptions = {
    origin              : 'http://localhost:4200',
    optionsSuccessStatus: 204,
    credentials         : true,
};

app.use(cors(corsOptions));
app.use(cookieParser());

app.use(
    '/graphql', graphqlHTTP({
        schema  : compiledSchema,
        graphiql: true,

    }),
);

app.use(fileUpload({
    useTempFiles: true,
    tempFileDir : '/tmp/',
}));

app.post('/upload', (req, res) => {
    console.log(req.files); // the uploaded file object
    res.send('File uploaded!');
});

app.listen(3000, () => console.log('API ready on port 3000'));
