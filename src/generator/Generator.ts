export class Generator {
    static getRandomInt(min = 1, max = 10) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    static checkRollInRange(roll, range) {
        const indexes = range.split('-').map(i => Number(i));
        if (indexes.length === 1) {
            return indexes[0] === roll;
        }
        for (let i = indexes[0]; i <= indexes[1]; i++) {
            if (roll === i) {
                return true;
            }
        }
        return false;
    }
}
