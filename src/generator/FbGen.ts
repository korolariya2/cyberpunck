import {Gen} from '../interfaces/Generator';
import {Profile} from '../entity/Profile';
import {FbLog} from '../entity/family background/FbLog';
import {Generator} from './Generator';
import {Fb} from '../entity/family background/Fb';
import {FbStep} from '../entity/family background/FbStep';

export class FbGen implements Gen {
    steps = [];

    profile: Profile;

    fbLogs: FbLog[] = [];

    async generate(profile): Promise<any> {
        this.profile = profile;
        await this.loadSteps();

        for (let index = 0; index < this.steps.length; index++) {
            const {log, roll} = await this.processStep(this.steps[index]);
            const fb          = await log.fb;
            if (fb.nextStep) {
                index = Number(fb.nextStep) - 1;
            }
            this.fbLogs.push(log);
            await log.save();
            if (fb.action && await this[fb.action](log, roll)) {
                return;
            }
        }

        this.profile.fbLog = this.fbLogs;
        return this.profile;
    }

    async processStep(step: string) {
        const roll = Generator.getRandomInt();
        const log  = await this.createLog(roll, step);
        return {log: log, roll: roll};
    }

    async loadData(step) {
        const i = String(this.steps.indexOf(step) + 1);
        return await Fb.find({
            where: {fbStep: i},
        });
    }

    private async createLog(roll: number, step: string): Promise<FbLog> {
        let data      = await this.loadData(step);
        const fbLog   = new FbLog();
        fbLog.name    = step;
        fbLog.profile = this.profile;
        fbLog.fb      = data.filter((item) => {
            return Generator.checkRollInRange(roll, item.roll);
        }).find(item => null != item);

        return fbLog;
    }

    async loadSteps() {
        this.steps = await FbStep.find();
        this.steps = this.steps.map((step) => {
            return step.name;
        });
    }

    async countSiblings(...data) {
        const [log, roll] = data;
        const siblings    = [];

        for (let i = 1; i <= roll; i++) {
            let sibling  = new Profile();
            sibling.name = 'sibling';
            this.choiceSex(sibling);
            this.choiceAge(sibling);
            this.coiceRelationship(sibling);
            siblings.push(sibling);
        }
        this.profile.siblings = siblings;
    }

    choiceSex(profile: Profile) {
        const roll  = Generator.getRandomInt();
        profile.sex = (roll % 2 == 0) ? 'male' : 'female';
    }

    choiceAge(profile: Profile) {
        const roll = Generator.getRandomInt();
        switch (true) {
            case roll >= 1 && roll <= 5:
                profile.age = 'older';
                break;
            case roll >= 6 && roll <= 9:
                profile.age = 'younger';
                break;
            case roll == 10:
                profile.age = 'twin';
                break;
        }
    }

    coiceRelationship(profile: Profile) {
        const roll = Generator.getRandomInt();
        switch (true) {
            case roll >= 1 && roll <= 2:
                profile.relationship = 'dislikes you';
                break;
            case roll >= 3 && roll <= 4:
                profile.relationship = 'likes you';
                break;
            case roll >= 5 && roll <= 6:
                profile.relationship = 'neutral';
                break;
            case roll >= 7 && roll <= 8:
                profile.relationship = 'worship you';
                break;
            case roll >= 9 && roll <= 10:
                profile.relationship = 'hate you';
                break;
        }
    }

    async stopPropagate(...date) {
        return true;
    }
}
