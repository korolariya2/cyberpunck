import {Generator} from './Generator';
import {Clothes} from '../entity/Clothes';
import {Profile} from '../entity/Profile';
import {Hairstyle} from '../entity/Hairstyle';
import {Affectations} from '../entity/Affectations';
import {EthnicOrigins} from '../entity/EthnicOrigins';

export class DPSGen {

    profile: Profile;

    public async create(profile: Profile) {
        this.profile = profile;
        await this.chooseClothes();
        await this.chooseHairstyle();
        await this.chooseAffectations();
        await this.chooseEthnicOrigins();
        return this.profile;
    }

    public async chooseClothes() {
        const roll           = Generator.getRandomInt(1, 10);
        this.profile.clothes = await Clothes.findOne({id: roll});
    }

    public async chooseHairstyle() {
        const roll             = Generator.getRandomInt(1, 10);
        this.profile.hairstyle = await Hairstyle.findOne({id: roll});
    }

    public async chooseAffectations() {
        const roll                = Generator.getRandomInt(1, 10);
        this.profile.affectations = await Affectations.findOne({id: roll});
    }

    public async chooseEthnicOrigins() {
        const roll                 = Generator.getRandomInt(1, 10);
        this.profile.ethnicOrigins = await EthnicOrigins.findOne({id: roll});
    }
}
