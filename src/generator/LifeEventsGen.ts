import {Profile} from '../entity/Profile';
import {LifeEvents} from '../entity/LifeEvents';
import {Generator} from './Generator';
import {BigProblemOrLuckyEvent} from './events/BigProblemOrLuckyEvent';
import {FriendsAndEnemiesEvent} from './events/FriendsAndEnemiesEvent';
import {RomanticInvolvementEvent} from './events/RomanticInvolvementEvent';
import {NothingHappenedEvent} from './events/NothingHappenedEvent';

export class LifeEventsGen {

    profile: Profile;

    events = {
        'bigProblemsOrLucky' : new BigProblemOrLuckyEvent(),
        'friendsAndEnemies'  : new FriendsAndEnemiesEvent(),
        'romanticInvolvement': new RomanticInvolvementEvent(),
        'nothingHappened'    : new NothingHappenedEvent(),
    };

    async generate(profile: Profile) {
        this.profile = profile;
        this.generateAge();
        const le = [];
        for (let i = parseInt(this.profile.age) - 16; i <= parseInt(this.profile.age); i++) {
            le.push(await this.choiceEvent());
        }

        this.profile.lifeEvents = le;
        return this.profile;
    }

    generateAge() {
        const roll       = Generator.getRandomInt(1, 12);
        this.profile.age = String(roll + 16);
    }

    async choiceEvent() {
        const roll    = Generator.getRandomInt(1, 10);
        const event   = new LifeEvents();
        event.profile = this.profile;
        for (let eventKey of Object.keys(this.events)) {
            await this.events[eventKey].execute(roll, event);
        }
        return event;
    }
}
