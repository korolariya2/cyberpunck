import {LifeEventAbstract} from './abstract/LifeEventAbstract';
import {Generator} from '../Generator';
import {Fae} from '../../entity/friends&enemies/Fae';
import {FaeLog} from '../../entity/friends&enemies/FaeLog';

export class FriendsAndEnemiesEvent extends LifeEventAbstract {
    name = 'Friends & Enemies';

    steps = [
        'Choice of sex',
        'Choice friend or enemy',
        'Make an enemy',
        'The cause',
        'Who\'s fracked off',
        'Whatcha\' gonna do about it?',
        'What can he throw against you',
        'Make a friend',
    ];

    itSuits(roll: number): boolean {
        return roll >= 4 && roll <= 6;
    }

    async generate() {
        await this.choiceOfSex();
    }

    async choiceOfSex() {
        const roll = Generator.getRandomInt(1, 10);
        if (roll % 2 == 0) {
            await this.createLog(0, 'Choice of sex');
        } else {
            await this.createLog(1, 'Choice of sex');
        }
        await this.choiceFriendOrEnemy();
    }

    async choiceFriendOrEnemy() {
        const roll = Generator.getRandomInt(1, 10);
        await this.createLog(roll, 'Choice friend or enemy');
        if (roll <= 6) {
            await this.makeFriend();
        } else {
            await this.makeEnemy();
        }
    }

    async makeEnemy() {
        const roll = Generator.getRandomInt(1, 10);
        await this.createLog(roll, 'Make an enemy');
        await this.choiceTheCause();
    }

    async makeFriend() {
        const roll        = Generator.getRandomInt(1, 10);
        this.event.faeLog = [await this.createLog(roll, 'Make a friend')];
    }

    async choiceTheCause() {
        const roll = Generator.getRandomInt(1, 10);
        await this.createLog(roll, 'The cause');
        await this.choiceWhoIsFrackedOff();
    }

    async choiceWhoIsFrackedOff() {
        const roll = Generator.getRandomInt(1, 10);
        await this.createLog(roll, 'Who\'s fracked off');
        await this.choiceWhatchaGonnaGoAboutIt();
    }

    async choiceWhatchaGonnaGoAboutIt() {
        const roll = Generator.getRandomInt(1, 10);
        await this.createLog(roll, 'Whatcha\' gonna do about it?');
        await this.choiceWhatCanHeThrowAgainstYou();
    }

    async choiceWhatCanHeThrowAgainstYou() {
        const roll = Generator.getRandomInt(1, 10);
        await this.createLog(roll, 'What can he throw against you');
    }

    async loadData(step) {
        const i = String(this.steps.indexOf(step) + 1);
        return await Fae.find({
            where: {faeStep: i},
        });
    }

    private async createLog(roll: number, step: string) {
        let data          = await this.loadData(step);
        const faeLog      = new FaeLog();
        faeLog.name       = step;
        faeLog.fae        = data.filter((item) => {
            return Generator.checkRollInRange(roll, item.roll);
        }).find(item => null != item);
        faeLog.lifeEvents = [this.event];
        await faeLog.save();
        return faeLog;
    }
}
