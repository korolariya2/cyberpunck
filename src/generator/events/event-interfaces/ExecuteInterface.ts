import {LifeEvents} from '../../../entity/LifeEvents';

export interface ExecuteInterface {
    execute(roll?: number, event?: LifeEvents): void;
}
