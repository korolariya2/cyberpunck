import {LifeEventAbstract} from './abstract/LifeEventAbstract';
import {Generator} from '../Generator';
import {BpBw} from '../../entity/big problems or big wins/BpBw';
import {BpBwLog} from '../../entity/big problems or big wins/BpBwLog';

export class BigProblemOrLuckyEvent extends LifeEventAbstract {
    name = 'Big Problems, Big Wins';

    steps = [
        'disasterStriker',
        'lucky',
        'whatAreYouGonnaDoAbout',
    ];

    itSuits(roll: number): boolean {
        return roll >= 1 && roll <= 3;
    };

    async generate() {
        const roll = Generator.getRandomInt(1, 10);
        if (roll % 2 == 0) {
            await this.setDisasterStrikers();
        } else {
            await this.setLucky();
        }
    }

    async loadData(step) {
        const i = String(this.steps.indexOf(step) + 1);
        return await BpBw.find({
            where: {bpBwStep: i},
        });
    }

    private async createBpBwLog(roll: number, step: string) {
        let data           = await this.loadData(step);
        const bpBwLog      = new BpBwLog();
        bpBwLog.bpBw       = data.filter((item) => {
            return Generator.checkRollInRange(roll, item.roll);
        }).find(item => null != item);
        bpBwLog.lifeEvents = [this.event];
        await this.runAction(bpBwLog);
        await bpBwLog.save();
        return bpBwLog;
    }

    async setDisasterStrikers() {
        let roll = Generator.getRandomInt(1, 10);
        await this.createBpBwLog(roll, 'disasterStriker');

        roll = Generator.getRandomInt(1, 10);
        await this.createBpBwLog(roll, 'whatAreYouGonnaDoAbout');
    }

    async setLucky() {
        const roll = Generator.getRandomInt(1, 10);
        await this.createBpBwLog(roll, 'lucky');
    }

    async runAction(bpBwLog: BpBwLog) {
        const bpBw    = await bpBwLog.bpBw;
        const actions = await bpBw.bpBwActions;

        if (actions.length) {
            const action = actions.filter((action) => {
                const roll = Generator.getRandomInt(1, 10);
                return Generator.checkRollInRange(roll, action.roll);
            }).find(item => null != item);

            bpBwLog.name = bpBw.name + action.name;
        }
    }

    debt(bpBwLog: BpBwLog) {
        console.log('debt');
        const roll   = Generator.getRandomInt(1, 10);
        const debt   = roll * 100;
        this.event.profile.cache -= debt;
        bpBwLog.name = 'You lose ' + debt + ' money.';
    }

    prison(bpBwLog: BpBwLog) {
        console.log('prison');
        let roll     = Generator.getRandomInt(1, 10);
        bpBwLog.name = 'You spent 10 months in prison.';
    }

    async illness() {
        console.log('Illness');
        const stats = await this.event.profile.stats;
        stats[0].ref -= 1;
    }

    betrayal() {
        console.log('betrayal');
        const roll = Generator.getRandomInt(1, 10);
        //TODO 1-3 шантаж 4-7 тайна раскрыта 8-10 предал друг кинул по карьерной линии
    }

    accident() {
        console.log('accident');
        //TODO roll 1-4 -5ATTR
        //TODO 5-6 (1D10) месяцев в госпитале
        //TODO 7-8 амнезия (1D10) месяцев воспоминаний стерто из памяти
        //TODO 9-10 постояно преследуют кошмары (8из10) ты просыпаешься с криком ужаса на устах
    }

    lover() {
        console.log('lover');
        //TODO 1-5 погиб в результате несчастного случая
        //TODO 6-8 Убит группой неизвестных
        //TODO 9-10 Был убит и ты знаешь кто это сделал. Но у тебя нет доказательств.
    }

    falseAccusation() {
        console.log('falseAccusation');
        //TODO 1-3 Обвинен в воровстве
        //TODO 4-5 В трусости
        //TODO 6-8 Убийстве
        //TODO 9 изнасиловании
        //TODO 10 лжи предательстве
    }

    huntedByTheLaw() {
        console.log('huntedByTheLaw');
        //TODO 1-3 Всего пара местных копов ловят тебя
        //TODO 4-6 Все местные силы
        //TODO 7-8 Полиция всего штата
        //TODO 9-10 FBI
    }

    huntedByACorporation() {
        console.log('huntedByACorporation');
        //TODO 1-3 Маленькая местная фирма
        //TODO 4-6 Большая корпарация
        //TODO 7-8 Большая национальная корпорация, масштаба страны
        //TODO 9-10 Огромная корпорация, со своей армией, убийцами и спец техникой
    }

    mentalOrPhysicalIncapacitation() {
        console.log('mentalOrPhysicalIncapacitation');
        //TODO 1-3 Это какое-то нервное расстройство, возможно из-за биовшивки -1 REF
        //TODO 4-7 Проблемы с психикой, страдаешь неврозами и фобиями -1 COOL
        //TODO 8-10 Это сильный психоз. Ты слышишь голоса, и чувствуешь сильную беспричинную депрессию -1 COOL -1 REF
    }

}
