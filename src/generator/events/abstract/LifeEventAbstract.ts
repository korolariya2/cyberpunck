import {LifeEvents} from '../../../entity/LifeEvents';
import {ExecuteInterface} from '../event-interfaces/ExecuteInterface';
import {SuitInterface} from '../event-interfaces/SuitInterface';

export abstract class LifeEventAbstract implements SuitInterface, ExecuteInterface {
    event: LifeEvents;

    name: string;

    async execute(roll: number, event: LifeEvents) {
        if (!this.itSuits(roll)) {
            return;
        }

        if (!event) {
            throw new Error('Should be you forgot param event.');
        }

        this.event = event;

        if (!this.name) {
            throw new Error('Should be you forgot set name.');
        }
        this.event.name = this.name;
        await this.event.save();
        await this.generate();
    }

    async generate() {
        throw new Error('Method not implemented.');
    }

    itSuits(roll: number): boolean {
        throw new Error('Method not implemented.');
    }
}
