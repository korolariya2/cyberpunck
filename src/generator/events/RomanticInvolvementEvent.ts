import {LifeEventAbstract} from './abstract/LifeEventAbstract';
import {Generator} from '../Generator';
import {RomanticLife} from '../../entity/romantic life/RomanticLife';
import {RomanticLifeLog} from '../../entity/romantic life/RomanticLifeLog';

export class RomanticInvolvementEvent extends LifeEventAbstract {
    name = 'Romantic Involvement';

    steps = [
        'How it worked out',
        'Love affair with problems',
        'Mutual feelings',
        'Tragic love affair',
    ];

    itSuits(roll: number): boolean {
        return roll >= 7 && roll <= 8;
    }

    async generate() {
        await this.choiceHowItWorkedOut();
    }

    async loadData(step) {
        const i = String(this.steps.indexOf(step) + 1);
        return await RomanticLife.find({
            where: {romanticLifeStep: i},
        });
    }

    private async createLog(roll: number, step: string) {
        let data                     = await this.loadData(step);
        const romanticLifeLog        = new RomanticLifeLog();
        romanticLifeLog.name         = step;
        romanticLifeLog.romanticLife = data.filter((item) => {
            return Generator.checkRollInRange(roll, item.roll);
        }).find(item => null != item);
        romanticLifeLog.lifeEvents   = [this.event];
        await romanticLifeLog.save();
        return romanticLifeLog;
    }

    async choiceHowItWorkedOut() {
        const roll = Generator.getRandomInt(1, 10);

        const romanticLifeLog = await this.createLog(roll, 'How it worked out');
        const romanticLife    = await romanticLifeLog.romanticLife;

        if (typeof this[romanticLife.action] == 'function') {
            await this[romanticLife.action]();
        }
    }

    async choiceLoveAffairWithProblems() {
        const roll = Generator.getRandomInt(1, 10);
        await this.createLog(roll, 'Love affair with problems');
    }

    async choiceTragicLoveAffair() {
        const roll = Generator.getRandomInt(1, 10);
        await this.createLog(roll, 'Tragic love affair');
        this.choiceMutualFeelings();
    }

    async choiceMutualFeelings() {
        const roll = Generator.getRandomInt(1, 10);
        await this.createLog(roll, 'Mutual feelings');
    }
}
