import {LifeEventAbstract} from './abstract/LifeEventAbstract';
import {Generator} from '../Generator';

export class NothingHappenedEvent extends LifeEventAbstract {
    name = 'Nothing happened';

    itSuits(roll: number): boolean {
        return roll >= 9 && roll <= 10;
    }

    async generate() {
    }
}
