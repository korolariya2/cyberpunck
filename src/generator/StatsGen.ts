import {Profile} from '../entity/Profile';
import {Stats} from '../entity/Stats';
import {Generator} from './Generator';

export class StatsGen {
    profile: Profile;

    statsKeys = [
        'int',
        'ref',
        'cool',
        'tech',
        'luck',
        'attr',
        'ma',
        'emp',
        'body',
    ];

    async generate(profile: Profile) {
        this.profile       = profile;
        const stats        = new Stats();
        this.profile.stats = [stats];
        this.fast(stats);
        stats.profile = this.profile;
        await stats.save();
    }

    fast(stats: Stats) {
        this.statsKeys.map((key) => {
            stats[key] = Generator.getRandomInt(3, 10);
        });

        stats.run   = stats.ma * 3;
        stats.leap  = stats.run / 3;
        stats.lift1 = stats.body * 10;
        stats.lift2 = stats.body * 40;
    }

    random() {
        // 9D10
    }

    cinematic() {

    }
}