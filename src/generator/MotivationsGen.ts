import {Profile} from '../entity/Profile';
import {Generator} from './Generator';
import {Motivation} from '../entity/motivations/Motivation';
import {MotivationsLog} from '../entity/motivations/MotivationsLog';
import {MotivationStep} from '../entity/motivations/MotivationStep';
import {asyncForEach} from '../LoaderFixtures';

export class MotivationsGen {
    steps = [];

    profile: Profile;

    motivationsLogs: MotivationsLog[] = [];

    async generate(profile: Profile) {
        this.profile = profile;
        await this.loadSteps();

        await asyncForEach(this.steps, async (step) => {
            await this.stepByStep(step);
        });

        this.profile.motivationsLog = this.motivationsLogs;
    }

    async loadSteps() {
        this.steps = await MotivationStep.find();
        this.steps = this.steps.map((step) => {
            return step.name;
        });
    }

    async stepByStep(step) {
        const roll = Generator.getRandomInt();
        const log  = await this.createLog(roll, step);
        this.motivationsLogs.push(log);
    }

    async loadData(step) {
        const i = String(this.steps.indexOf(step) + 1);
        return await Motivation.find({
            where: {motivationStep: i},
        });
    }

    private async createLog(roll: number, step: string) {
        let data                  = await this.loadData(step);
        const motivationsLog      = new MotivationsLog();
        motivationsLog.name       = step;
        motivationsLog.profile    = this.profile;
        motivationsLog.motivation = data.filter((item) => {
            return Generator.checkRollInRange(roll, item.roll);
        }).find(item => null != item);
        await motivationsLog.save();
        return motivationsLog;
    }
}
